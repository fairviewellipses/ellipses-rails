class AddSlugToSectionsAndStories < ActiveRecord::Migration[5.0]
  def change
    add_column :stories, :slug, :string
    add_column :sections, :slug, :string
  end
end
