class AddSlugToStories < ActiveRecord::Migration[5.0]
  def change
    add_column :stories, :slug, :string
  end
end
