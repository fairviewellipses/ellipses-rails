class AddEmailAndCommentsToStories < ActiveRecord::Migration[5.0]
  def change
    add_column :stories, :email, :string
    add_column :stories, :comments, :string, default: ""
  end
end
