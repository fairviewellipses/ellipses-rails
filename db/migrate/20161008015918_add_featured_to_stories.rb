class AddFeaturedToStories < ActiveRecord::Migration[5.0]
  def change
    add_column :stories, :featured, :boolean
  end
end
