class AddApprovedToStories < ActiveRecord::Migration[5.0]
  def change
    add_column :stories, :approved, :boolean, default: false
  end
end
