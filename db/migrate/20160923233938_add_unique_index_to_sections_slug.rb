class AddUniqueIndexToSectionsSlug < ActiveRecord::Migration[5.0]
  def change
    add_index :sections, :slug, unique: true
  end
end
