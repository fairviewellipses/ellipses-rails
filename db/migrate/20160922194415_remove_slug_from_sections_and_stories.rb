class RemoveSlugFromSectionsAndStories < ActiveRecord::Migration[5.0]
  def change
    remove_column :stories, :slug
    remove_column :sections, :slug
  end
end
