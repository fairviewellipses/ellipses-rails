class RenameApprovedToStoryApproved < ActiveRecord::Migration[5.0]
  def change
    rename_column :stories, :approved, :story_approved
  end
end
