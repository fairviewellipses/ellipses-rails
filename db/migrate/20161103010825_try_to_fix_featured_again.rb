class TryToFixFeaturedAgain < ActiveRecord::Migration[5.0]
  def change
    change_column :stories, :featured, :boolean, null: false, default: false
  end
end
