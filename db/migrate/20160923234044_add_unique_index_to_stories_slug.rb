class AddUniqueIndexToStoriesSlug < ActiveRecord::Migration[5.0]
  def change
    add_index :stories, :slug, unique: true
  end
end
