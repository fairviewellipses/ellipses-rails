class AddGalleryLayoutToStories < ActiveRecord::Migration[5.0]
  def change
    add_column :stories, :gallery_layout, :boolean, default: true
  end
end
