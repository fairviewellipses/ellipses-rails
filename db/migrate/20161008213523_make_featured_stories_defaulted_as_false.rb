class MakeFeaturedStoriesDefaultedAsFalse < ActiveRecord::Migration[5.0]
  def change
    change_column :stories, :featured, :boolean, :default => false
  end
end
