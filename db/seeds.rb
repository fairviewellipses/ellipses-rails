def generate_content(paragraphs, sentences_per_paragraphs)
  result = ""
  paragraphs.times do
    result += "#{Faker::Lorem.paragraph(sentences_per_paragraphs)}\n"
  end
  return result
end

Section.create(name: 'short stories')
Section.create(name: 'poetry')
Section.create(name: 'art')

3.times do
  Section.where(name: 'short stories').first().stories.create!(title: Faker::Book.title,
              genre: Faker::Book.genre, content: generate_content(10, Random.rand(10) + 5),
              author: Faker::Book.author, email: "example@example.com", story_approved: true,
              featured: false)
end

s = Section.where(name: 'short stories').first().stories.all.sample()
s.featured = true
s.save!

3.times do
  Section.where(name: 'poetry').first().stories.create!(title: Faker::Book.title,
              genre: Faker::Book.genre, content: generate_content(5, Random.rand(1) + 3),
              author: Faker::Book.author, email: "example@example.com", story_approved: true,
              featured: false)
end

s = Section.where(name: 'poetry').first().stories.all.sample()
s.featured = true
s.save!

User.create!(email: "admin@example.com", name: "admin", password: "asdfasdf",
            password_confirmation: "asdfasdf", approved: true)
