# config valid only for current version of Capistrano
lock '3.11.0'

set :application, 'ellipses-rails'
set :repo_url, 'git@gitlab.com:fairviewellipses/ellipses-rails.git'

set :user, 'deploy'
set :ssh_options, user: 'deploy', forward_agent: true

set :rails_env, 'production'
set :rvm_ruby_version, '2.4.0'

set :pty, false

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system')

after :deploy, :'passenger:restart'

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
