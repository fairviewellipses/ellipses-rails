Rails.application.routes.draw do
  resources :users, except: [:new]
  get '/signup', to: 'users#new'

  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  put '/users/:id/approve', to: 'users#approve', as: 'approve'

  root 'static_pages#home'

  get '/about', to: 'static_pages#about'
  get '/uploading', to: 'static_pages#uploading'

  resources :stories

  patch '/stories/:id/feature', to: 'stories#feature', as: 'feature'

  patch '/stories/:id/story_approve', to: 'stories#story_approve', as: 'story_approve'

  resources :sections

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
