class Section < ApplicationRecord
  has_many :stories
  extend FriendlyId
  friendly_id :name, use: :slugged
end
