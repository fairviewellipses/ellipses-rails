class User < ApplicationRecord
  validates :name, presence: true, length: {maximum: 100}

  validates :password, presence: true, confirmation: true, length: {minimum: 6}, allow_nil: true

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: {with: VALID_EMAIL_REGEX}, uniqueness: true

  has_secure_password
end
