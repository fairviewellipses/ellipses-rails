class Image < ApplicationRecord
  belongs_to :story

  has_attached_file :image
  validates_with AttachmentPresenceValidator, attributes: :image
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
