class Story < ApplicationRecord
  validates :title, presence: true, length: {maximum: 200}
  validates :author, presence: true, length: {maximum: 100}
  validates :genre, presence: true, length: {maximum: 100}
  validates :email, presence: true, format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i }, length: {maximum: 255}

  class ImagesValidator < ActiveModel::Validator
    def validate(record)
      if record.images.empty? and record.content.empty? and record.section.name == 'art'
        images.errors << 'need at least one image or some content for art submissions'
      end
    end
  end
  validates_with ImagesValidator

  belongs_to :section
  has_many :images, dependent: :delete_all

  extend FriendlyId
  friendly_id :title, use: :slugged

  def content_to_html
    self.content.gsub(/\n/, '</p><p>')
  end
end
