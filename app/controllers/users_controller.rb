class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :approve]
  before_action :user_security
  before_action :approved_user_security, only: [:approve]
  before_action :current_user_security, only: [:edit, :update]
  before_action :current_user_or_approved_user_security, only: [:destroy]

  # GET /users
  def index
    @users = User.all
  end

  # GET /users/1
  def show
  end

  # GET /signup
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      flash[:success] = "#{@user.name} was successfully created"
      redirect_to @user
    else
      render :new
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      flash[:success] = "#{@user.name} was successfully updated"
      redirect_to @user
    else
      render :edit
    end
  end

  # PATCH/UPDATE /users/1/approve
  def approve
    if @user.approved
      @user.approved = false
      string = 'unsapproved'
    else
      @user.approved = true
      string = 'approved'
    end

    if @user.save!
      flash.now[:success] = "#{@user.name} was successfully #{string}."
      redirect_to users_path
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    flash[:success] = "#{@user.name} was deleted."
    redirect_to users_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:email, :name, :approved, :password, :password_confirmation)
    end

    def current_user_security
      @user = User.find(params[:id])
      if @user != current_user
        flash.now[:danger] = @@security_message
        redirect_to root_path
      end
    end

    def current_user_or_approved_user_security
      if current_user != @user || !user.approved
        flash.now[:danger] = @@security_message
        redirect_to root_path
      end
    end
end
