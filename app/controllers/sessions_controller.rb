class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password]) && user.approved
      log_in user
      flash[:success] = 'Successfully logged in'
      redirect_to user
    else
      flash.now[:danger] = 'Invalid email/password combination (or the user isn\'t approved)'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_path
    flash[:success] = 'Successfully logged out'
  end
end
