class StoriesController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :destroy, :story_approve]
  attr_accessor :featured

  def show
    @story = Story.friendly.find(params[:id])
    @content = @story.content_to_html
  end

  def edit
    @story = Story.friendly.find(params[:id])
  end

  def update
    @story = Story.friendly.find(params[:id])
    if @story.update_attributes(story_params)
      flash[:success] = "\"#{@story.title}\" was successfully updated."
      redirect_to @story
    else
      render 'edit'
    end
  end

  def story_approve
    @story = Story.friendly.find(params[:id])
    if @story.story_approved
      @story.story_approved = false
      string = 'disapproved'
    else
      @story.story_approved = true
      string = 'approved'
    end

    if @story.save!
      flash.now[:success] = "#{@story.title} was successfully #{string}."
      redirect_to @story
    end
  end

  def destroy
    @section = Story.friendly.find(params[:id]).section
    Story.friendly.find(params[:id]).destroy
    flash[:success] = "Story deleted."
    redirect_to @section
  end

  def feature
    @story = Story.friendly.find(params[:id])
    @prev_featured = @story.section.stories.find_by(featured: true)

    if @prev_featured
      @prev_featured.featured = false
      @prev_featured.save!
    end

    @story.featured = true

    if @story.save
      flash[:success] = "\"#{@story.title}\" is now featured."
      redirect_to root_url
    else
      flash[:danger] = "Something went wrong."
      redirect_to @story.section
    end
  end

  def new
    @story = Story.new
  end

  def create
    @story = Story.new(story_params)
    @story.featured = false

    if params[:story][:images]
      params[:story][:images].each do |image|
        @story.images.new(image: image)
      end
    end

    if @story.save
      flash[:success] = "\"#{@story.title}\" was successfully submitted"
      redirect_to @story
    else
      render :new
    end
  end

  private
  def story_params
    params.require(:story).permit(:title, :author, :content, :genre, :section_id, :anonymous, :comments, :email, :story_approved, :featured, :images, :gallery_layout)
  end

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in to do this."
      redirect_to login_url
    end
  end
end
