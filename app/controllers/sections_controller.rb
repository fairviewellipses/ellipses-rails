class SectionsController < ActionController::Base
  include SessionsHelper
  layout "application"
  def show
    @section = Section.friendly.find(params[:id])
    if(current_user && current_user.approved)
      @stories = @section.stories.sort_by {|story| story.created_at}
    else
      @stories = @section.stories.where(story_approved: true).sort_by {|story| story.created_at}
    end
  end
end
