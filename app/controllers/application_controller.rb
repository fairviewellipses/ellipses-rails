class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  @@login_message = 'You must be logged in to access that page'
  @@security_message = 'You are not allowed to access that page'
  @@approved_message = 'You must be approved to access that page'

  def user_security
    unless logged_in?
      flash[:danger] = @@login_message
      redirect_to login_url
    end
  end

  def approved_user_security
    unless current_user.approved
      flash[:danger] = @@approved_message
      redirect_to root_path
    end
  end
end
