class StaticPagesController < ApplicationController
  def home
    @featured_stories = []
    Section.all.each do |section|
      story = section.stories.find_by(featured: true)
      if !story
        story = section.stories.first
      end
      @featured_stories << story unless story == nil
    end
  end

  def about
  end

  def uploading
  end
end
