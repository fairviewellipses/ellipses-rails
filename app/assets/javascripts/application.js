// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require tinymce
//= require fancybox
//= require_tree .

setTimeout(function() {
  $(document).ready(function() {

    if($(".inner-content").hasClass("gallery-true")) {
      $(".story-img, .inner-content > p > img").addClass("fancybox thumbnail");
      $(".fancybox").each(function(i) {
        $(this).attr("href", $(this).attr("src"));
      });
      $(".fancybox").fancybox();
    }

    $(".story-title, .inner-content").removeClass("hidden");

    var checkStoryType = function() {
      if($("#story_section_id").find(":selected").text() === "Art") {
        $(".art-field").removeClass("hidden");
     } else {
        $(".art-field").addClass("hidden");
     }
   };

    checkStoryType();

    $("#story_section_id").change(function() {
      checkStoryType();
   });

   $("p").has("img").each(function() {
     $(this).css("display", "inline-block");
     $(this).css("text-indent", "0");
     $(this).css("margin-top", "0");
   });


    $("#bottom-ellipses").click(function() {
      $('html, body').animate({
        scrollTop: 0
     }, 500);
   });

    $("#hamburger").click(function() {
      $("#hamburger, #header-links").toggleClass("expanded");
   });
 });
}, 10);
