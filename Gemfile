source 'https://rubygems.org'

ruby '2.4.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '5.0.1'

# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'

# Use SCSS for stylesheets
gem 'sass-rails'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier'

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails'

# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
# Turbolinks are disabled due to issues with fancybox.
# gem 'turbolinks'

# Use Redis adapter to run Action Cable in production
gem 'redis'

# Use ActiveModel has_secure_password
gem 'bcrypt'

# For example story seeding
gem 'faker'

# Use slugs for some urls
gem 'friendly_id' # Note: You MUST use 5.0.0 or greater for Rails 4.0+

# Use paperclip for working with files
gem 'paperclip'

# Use tinymce for wysiwyg implementation
gem 'tinymce-rails'

# Fancybox for galleries
gem 'fancybox-rails'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console'
  gem 'listen'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen'

  # Use Thin as the development app server
  gem 'thin'

  # Use capistrano for deploying
  gem 'capistrano'
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano-bundler'
  gem 'capistrano-passenger'
  gem 'capistrano-faster-assets'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
